// Classifier Variable
  let classifier;
  // Model URL
  //let imageModelURL = 'https://teachablemachine.withgoogle.com/models/8OKM5ils-/';
  let imageModelURL = 'https://teachablemachine.withgoogle.com/models/Ty-s0dDOE/';

var video;
var button;

let label = "";
let confidence = "";


function preload() {
  classifier = ml5.imageClassifier(imageModelURL + 'model.json');
}

function setup() {
  // put setup code here
  createCanvas(320,260);
  background(51);

  video = createCapture(VIDEO);
  video.size(320,240);
  button = createButton('capture');
  button.mousePressed(takesnap);



  //flippedVideo = ml5.flipImage(video);
  classifyVideo();

}

function takesnap(){
  image(video,0,0)
  //history.push(video.get());
  classifyVideo();



  fill(51)
  strokeWeight(5);
  stroke(0);
  rect(0,240,320,20);

    fill(255);
    textSize(16);
    textAlign(CENTER);
    text(label, width / 2, height - 4);

    fill(255);
    textSize(16);
    textAlign(CENTER);
    text(confidence, width / 12, height - 4);





}


function classifyVideo() {
  //flippedVideo = ml5.flipImage(video)
  classifier.classify(video, gotResult);
  //flippedVideo.remove();

}

function draw() {

}


// When we get a result
function gotResult(error, results) {
  // If there is an error
  if (error) {
    console.error(error);
    return;
  }

  // createButtonconsole.log(results);
  // The results are in an array ordered by confidence.
  // console.log(results[0]);
  label = results[0].label;
  confidence = nf(results[0].confidence,0,2);
  //noLoop();
    //confidence.html('Confidence: ' + nf(results[0].confidence, 0, 2)); // Round the confidence to 0.01
  // Classifiy again!
  classifyVideo();
}

function mousePressed(){
  console.log(mouseX, mouseY);
}
