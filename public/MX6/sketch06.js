let bubbles=[];
let counter = 0;

function setup() {
  //put setup code here
  print("hello world!");
  createCanvas(900,600);

  for (let i = 0; i < 0; i++){
    let x = random(width);
    let y = random(height);
    let r = random(20,50);
    bubbles[i] = new Bubble(x,y,r);
  }

}

function draw() {
  //put drawing code here
  background(0);
    for (b of bubbles){
      b.show();
      b.move();

      }



fill(255);
textSize(20)
text("Click to add bubbles",20,550);
text("There are " + counter + " bubble(s)", 20, 580);

if (mouseIsPressed){
  console.log(mouseX,mouseY);
}

}

function mousePressed(){
  let r = random(20,50);
  let b = new Bubble(mouseX,mouseY,r);
  bubbles.push(b);
  counter++;
}

class Bubble{
  constructor(x,y,r){
    this.x = x;
    this.y = y;
    this.r = r;
    this.brightness = 255;
  }

  move(){
    this.x+=random(-5,5);
    this.y+=random(-5,5);
    let d = dist(mouseX, mouseY,this.x, this.y);
    if (d < this.r){
      background(240,0,0);
      console.log('Error');
    }
  }

  show(){
    noStroke();
    strokeWeight(3);
    fill(this.brightness,90);
    circle(this.x,this.y,this.r*2);
  }

  changeColor(bright){
    this.brightness = bright;
  }

}
