<img src="bubbles.png" width="400" height="290"> <img src="notworking.png" width="400" height="290">

[Click for bubbles](https://pernwn.gitlab.io/ap2020/MX6/)

[Repository](https://gitlab.com/pernwn/ap2020/-/tree/master/public%2FMX6)

## Describe how does your game/game objects work?
This program is not particularly a game, but the principles of it can be understood the same way as “cookie clicker”. In this you have to repeatedly click on a cookie and the score will go up. The same applies for this program, where you have to click on the canvas in order to create more and more bubbles. Upon hovering on the bubbles, the canvas will turn red. I made this to make somewhat of a reflection of “data capturing”. My idea is that we always provide our digital artefacts with data, which is collects and store in a database. Over time, the capacity of the database will be overloaded with data and errors occur. Though, sometimes we do not see these errors explicitly, but by making the canvas of this program change in colour, shows the data creating error on your device. 

This was a quickly made up reflection as this was not my initial idea. I wanted to make a simple game, with no wider cultural meaning. The idea of the game was that you had to use the arrow keys to get a yellow rectangle, which I called “Buddy”, to the other side. Buddy has to avoid the floating objects in order not to die. Whenever the user gets to the other side, the game would reset. There would be no scores and nothing but one’s own desire to “cross the road”. 

I have uploaded the sketch in the folder called “GameNotWorking”. I couldn’t get it to work in terms of spawning endless amounts of the objects which Buddy has to avoid. I wanted them to appear at different Y-positions and have every second row go in the opposite direction than the previous row. So, row 1 would move from left to right, row 2 would move from right to left and so on. I also couldn’t figure out how to reset the game whenever Buddy would touch one of the objects. I have created a game before where I could make it work, but I didn’t know how in this program because I made the objects by using class and array. 

## Describe how you program the objects and their related attributes and methods in your game.
For the bubbles I followed alongside Daniel Shiffman’s videos in which he talks of arrays, classes etc. I tried to combine certain syntaxes to make it my own and not just take from Shiffman. 

The bubbles are made in a class with a function called move to make them move randomly on the canvas: this.x+=random(-5,5) and this.y+=random(-5,5). In this function I also set the distance (mouseX, mouseY, this.x, this.y) and made a conditional statement: if(d < this.r) = “do something”. Here I implemented the changing background. So, the program is figuring out if the distance of the mouse is within any of the bubbles, then it will execute the task I have given it. 

There is also a function called changeColor. I made this with the intention of making the bubbles change colour everytime they intersect each other. I tried to follow along Daniel Shiffman’s video, but I couldn’t make the “intersects” function work properly like he did in the video. 

So, the only interaction in this game is whenever the user clicks on the canvas to create bubbles. Also, when the user is hovering on one of the bubbles, thus making the program execute the given task of changing the background colour. 

## What are the characteristics of object-oriented programming and the wider implications of abstraction?

I want to mention the text we read by Nick Montford: “Appendix A: Why Program?”. He mentions that programming helps us think. He also mentions Engelbart which said that those who are using computers as a tool should understand computer programming (p. 270). It is this idea with abstraction. For you to operate a certain artefact, you need to know which methods of the objects are available to call and which input parameters are needed to trigger a specific operation. What you don’t need to understand, is how this method is implemented and which kinds of actions it has to perform to create the expected result.

You can easily play a game because you know the methods that will make the game function properly, like using arrow keys or spacebar. What you don’t know, and don’t really need to know, is how it is possible for you to use the arrow keys to trigger a specific operation. But, for those who work in the industry of for example game design, has to know these “hidden” operations within a game. To create something, you have to understand how it behaves, what features are available and how to make it execute certain tasks. 

## Extend/connect your game project to wider digital culture context, can you think of a digital example and describe how complex details and operations are being abstracted?
The only requirement in my program is that you know how to operate a mouse or trackpad on a computer. The only method in the game is clicking and moving the mouse around. The clicking can be connected with that of clicking when taking a picture. Often, people make clicking noises when pretending to take a picture when an invisible camera. I have a digital camera which can have different settings. The abstraction in the camera is how, by clicking on a button, the camera takes a picture and stores it in a SD card. 

Let’s say this: you know how to take a picture with a digital camera. You know how to change the settings to make the picture better. You know how to make the flashlight appear when taking a picture etc. What you don’t know is how the camera works internally to make your actions possible. What goes on inside the camera whenever you click the button? You don’t know because someone else worried about that for you, in order for you to have the possibility to operate such a camera. 




