let buddySize  = {
  w:88,
  h:88,
};
let logs = [];
let min_logs = 4;
let buddyPosY;
let buddyPosX;
let posSpeed  = 90;
let gameEnd = false;
let score = 0;
let buddy;


function setup() {
  //put setup code here
  print("hello world!");
  createCanvas(1000,800)
  frameRate(20);
  buddyPosY = 721;
  buddyPosX = 500;

  for (let i = 0; i <= width; i++){
    let x = 10 + 400 * i;
    let y = 633;
    logs[i] = new Logs(x,y);
  }



}
function draw() {
  //put drawing code here
  background(0);


  //the ground
  for (var y1 = 0; y1 <= height; y1+=180){
    fill(255,40);
    noStroke(0);
    rect(0,y1,1000,90);
  }

  //object to move across the canvas
  fill(255,255,0);
  noStroke();
  buddy = rect(buddyPosX,buddyPosY,buddySize.w,buddySize.h);


  showLogs();
  checkLogsnum();
  gameReset();
  // checkTouch();

  fill(255);
  textSize(15);
  noStroke();
  text('Move cube with arrow keys',20,740);
  text('Restart game with spacebar',20,760);

  if (mouseIsPressed){
  console.log(mouseX,mouseY);
  }
}

// function checkTouch(){
//   for (let i = 0; i < logs.length; i++){
//     let d = dist(buddySize.w, buddyPosY+buddySize.h,logs[i].x, logs[i].y);
//     if (d < buddySize.w && buddySize.h){
//       score++;
//       logs.splice(i,1);
//       console.log("working");
//     }
//   }
// }

function gameReset(){
  if (buddyPosY < 40){
    gameEnd = true;
  }

  if(gameEnd){
    buddyPosY = 721;
    buddyPosX = 500;
  }
}

function keyPressed(){
  if (keyCode === UP_ARROW){
    buddyPosY-=posSpeed;
  } else if (keyCode === DOWN_ARROW){
    buddyPosY+=posSpeed;
  }else if (keyCode === LEFT_ARROW){
    buddyPosX-=posSpeed;
  }else if (keyCode === RIGHT_ARROW){
    buddyPosX+=posSpeed;
  }else if (keyCode === 32){
    gameEnd = false;
  }

}

function showLogs(){
  for(let i = 0; i < logs.length; i++){
    logs[i].move();
    logs[i].show();
  }
}

function checkLogsnum(){
  if (logs.length <= min_logs){
    logs.push(new Logs());
  }
}

class Logs{
  constructor(x,y){
    this.x = x;
    this.y = y;
  }

  move(){
    this.x+=5;
    // this.y+=random(-5,5);
  }

  show(){
    stroke(255);
    strokeWeight(3);
    fill(255);
    rect(this.x,this.y,190,85);
  }
}
