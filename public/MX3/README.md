![screenshot](coffeethrobber.png)

[Link to throbber](https://pernwn.gitlab.io/ap2020/MX3/)

[Link to repository](https://gitlab.com/pernwn/ap2020/-/tree/master/public/MX3)

<h2>Describe your throbber design, both conceptually and technically.</h2>

In this miniEx I haven't put as much thought into it as the last one. Initially I wanted to make a coffee mug with steam coming out off of the top of the mug. 
I thought I had to use the syntax vertex again, which I used alot in miniex2, so I chose a different approach instead. Now the mug gets filled with coffee
and the coffee also disappears – as if someone is drinking it. I wanted to express the fact that when we want to relax or just take a break from work or studies,
we say that we are taking a coffee break. Then making a throbber symbolising a coffee, thus symbolising the coffee break, it kind of destroys the idea 
of the concept being a relaxing time. People do not want to wait for their screen to load, they want things to run smoothly and fast. And when introducing 
a symbol of relaxation into something which can be so frustrating, is not particularly funny. "[...] most people in everyday life do not want to see it [throbber] show on their screens as it represents slowness and interruption" (Soon, W. 2019:9)

But on the other side, it can also be seen as implementing a symbol of relaxation might decrease the frustration one might experience when waiting for a screen to load, 
when given no information to the progress whatsoever. Though, this might be up for interpretation by the individual person. 


I had a lot of problems with this, as I didn't know how to make the coffee loop up and down. Like, increasing and decreasing the heigt of the "coffee rectangle".
I tried to use different syntaxes in my efforts to make the coffee increase and decrease, but nothing seemed to be working for me. 
I went through some of the repositories of the people in the class and found Jonas' syntax very interesting. I read his sketch and tried to put it into my own program. 
I tried to  modify it to the best of my abilities and when more problems occured and I went over his program and changed som variables and numbers, in trying to figure out what everything meant and was supposed to do. 
Eventually I figured it out and it seems to be working perfectly. I haven't fully understood the syntax yet, but I am still fiddling with it. 