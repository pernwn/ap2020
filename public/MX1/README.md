![screenshot](https://i.imgur.com/6gpLXgN.png) ![screenshot](rainbowstairs.png)

[link](https://pernwn.gitlab.io/ap2020/MX1/)

**Questions to think about in your README:**

**How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**

*  After learning the basic functions it seemed pretty straightforward but as I wanted to manipulate the codes and merging different codes together,
it was really frustrating. I couldnt figure out where to put the different functions and how the codes would  interact with eachother. 
I tried to modify some more complex codes, but I all I knew how to change were the different numbers – the colour and placement of the shapes. I made
two individual sketches and tried to figure out how to combine them but to no avail. It was irritating and I eventually gave up in trying to do so. Though,
I will definitely keep on trying to copy, modify and write different codes together. 
    

**How is the coding process different from, or similar to, reading and writing text?**

*  Once you get to know the different commands you'll be able to write them as easily as writing a regular text. Though, in the beginning, it is almost
like a never ending frustration between going back and forth from code to code. When writing words, especially in text messages, in doesn't really matter
if you make a typo. In coding, you have to write the commands exactly right in order for it to work properly. You also need to be aware of which code
goes where and if there is any correlation between the different codes. 

**What does code and programming mean to you, and how do the assigned readings help you to reflect on programming?**

*  Coding something which you think is great in your own head, isn't as straightforward as you might think. In my case I had all kinds of ideas but I didn't
know which commands to use in order for it to work. I have a great respect for programming. From the assigned readings I gained an understanding of how
programming can be used to collaborate with various teams of proffesion. Not only in the field of primarily computing, but it is also a great tool for artists
and humanists to aquire. 