## Flowchart for miniex4 (new version because the previous had an "error")
<img src="obsession(3)2.png"  width="450" height="750">


[Link to the first idea (API)](https://drive.google.com/file/d/13ZxDOa2tWY0Wt7S2iI0gJsbeVHcy4pHI/view?usp=sharing)

[Link to the second idea (Machine Learning)](https://drive.google.com/file/d/1DGWd6UNN0TOu15beno0jDFYxPcA_1tye/view?usp=sharing)

*The links should work, otherwise go to Anne Nielsens, Torvalds or Simons GitLab*

## What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure?

Using flowcharts in describing a programming process can have both its advantages and its disadvantages. They’re used for visualising a complex structure of a program or software. Mostly it should be written before the programming takes place although many often write them after the fact. 

When written “correctly” it can communicate between programmers, users, managers etc. It’s comes down to the fact of how simple or complex it has been written. In general, the flowcharts can be thought of as a boundary object; “An artifact that simultaneously inhabits multiple intersecting social and technical worlds” (Susan Starr and James Griesemer in Ensmenger 2016:324). So, the flowcharts, as boundary objects, works in between different social worlds; as it is a form of communication of the technical aspects worded to be understood in a non-technical world. To put it in another perspective, Kathryn Henderson has applied the term, boundary objects, to the sketches and drawings which are used between engineers to communicate among themselves and others (Ensmenger 2016). To jot it all down, the flowcharts in a programming process is used to visualise the source code at hand by non-programmers and programmers alike. 

Although, Ned Chapin has suggested that a flowchart with too much detail were no more useful than the code itself (Ensmenger 2016). Donald Knuth then mocks the oversimplified flowcharts, as they were often wrong, and it would then again be easier to look at the code (Ensmenger 2016).

## What are the technical challenges for the two ideas and how are you going to address them?

The ideas we have discussed in our group are working around the themes of API and Machine Learning. The first one, which is that of APIs, is kind of a search engine. We worked around the idea of an API is like a waiter in a restaurant, because a waiter suggests dishes (data) from a menu (list of data), and the waiter chooses what you want to eat and keeps the other dishes “hidden”. 

You write in what type of dish you want in the search bar then the program will generate 3-5 randomly picked dishes from an API we have chosen. So, the dishes shown may not be what the user particularly wanted, but it is what the waiter (API) had suggested. At this point what I believe would be challenging, is to find the right API that contains the information that we would like to implement. What important is a broad API in the sense that it contains different varieties of dishes. Another challenge is to figure out how to make the search engine generate random dishes, which are not determined by what the user has searched for. We also want to implement a gimmick of some sort, like a glitch in the system where there’s a 15% probability of the text “What would you like to eat” being replaced with “What data do you want?” which will only be visible for half a second at random. Which I believe will also be a challenge for us. As seen on our flowchart, we have only implemented the conceptual idea, so it is short without any technical aspects, which you will also see on the second flowchart.

The second idea we had is something in the field of Machine Learning and Artificial Intelligence. We want to make a musical program of some sort. The idea is that the user is presented with 6 buttons, each producing a different sound. The user has to click the buttons in any pattern that they wish to. At the end, the user can choose to generate these different sounds into a compiled melody. The thing is; the program will remember the user inputs given and at given point the program will generate its own melodies without the user having clicked anything. The program will have learned from the user inputs to generate any random melodies from the sounds and can choose to mix and match as it goes. I believe that making the buttons and assigning a different sound/tune to each will be the easiest of the whole program. What might be challenging is figuring out how to make the program generate the melodies and, in the end, generate its own melodies without user inputs. 

## What is the value of the individual and the group flowchart that you have produced?

I feel there’s a completely different thinking process going on when having to write a flowchart prior to the program making and after the fact. When making the group flowchart we didn’t account for any code or if it was even possible to make such a program. I feel like it was a tool for brainstorming and generating ideas – without limits (yet). The flowcharts we generated are much more conceptual than technical, as we thought of the process of interacting with the programs rather than the programs process when interacted with. The group flowcharts might change as we begin to write the code, but the individual flowchart remain the same. To me, it feels more like a summary of what I had made weeks ago. It’s different than the group flowcharts though as I feel I could implement more details of the program. 

## Sources

*  Ensmenger, Nathan. "The Multiple Meanings of a Flowchart." Information & Culture: A Journal of History, vol. 51 no. 3, 2016, pp. 321-351. Project MUSE, doi:10.1353/lac.2016.0013
