let value=('#CDECFF');
let brainstrokeW=6;
let brainlines=8;

function setup() {
  //put setup code here
  createCanvas(1600,900);
  print("hello world!");
  background(183,109,231);
}
function draw() {
  //put drawing code here

  //background for first emoji
  fill(26,97,210);
  noStroke();
  rect(0,0,800,9000)

  //emoji 1: phone with brain
  //phone body
  fill(0);
  noStroke();
  rect(250,150,300,500,20);

  //phone screen
  fill(value);
  strokeWeight(1);
  stroke(121,131,136);
  rect(265,200,270,400,5);

  //phone button
  strokeWeight(1);
  stroke(121,131,136);
  fill(0);
  circle(400,625,38,38);

  //phone camera lens
  strokeWeight(1);
  circle(400,180,10,20);

  //brain
  fill(238,190,201);
  stroke(245,161,180);
  strokeWeight(brainlines);
  strokeJoin(ROUND);
  beginShape();
  //left side of brain
    vertex(400,295);
    bezierVertex(350,280,340,320,337,322);

    vertex(337,322);
    bezierVertex(295,330,295,373,296,368);

    vertex(297,364);
    bezierVertex(270,410,340,430,400,410);

  //right side of brain
    vertex(400,295);
    bezierVertex(430,290,445,295,462,324);

    vertex(462,324);
    bezierVertex(500,330,498,375,497,364);

    vertex(497,364);
    bezierVertex(530,410,440,430,400,410);
  endShape();

  //lines inside of brain
  noFill();
  stroke(231,153,171);
  strokeWeight(brainstrokeW);
  strokeJoin(ROUND);
  //lines left side
  beginShape();
    curveVertex(340, 240);
    curveVertex(330, 329);
    curveVertex(367, 353);
    curveVertex(370, 390);
    curveVertex(348, 413);
    curveVertex(330, 490);
  endShape();

  beginShape();
    curveVertex(370,400);
    curveVertex(353,362);
    curveVertex(358,348);
    curveVertex(358,348);
  endShape();

  beginShape();
  curveVertex(600,200);
  curveVertex(395,296);
  curveVertex(378,326);
  curveVertex(385,315);
  endShape();

  beginShape();
  curveVertex(299,390);
  curveVertex(298,390);
  curveVertex(309,377);
  curveVertex(330,387);
  curveVertex(335,375);
  curveVertex(322,389);
  endShape();

  //lines right inside
  beginShape();
  curveVertex(350,299);
  curveVertex(420,295);
  curveVertex(412,334);
  curveVertex(452,354);
  curveVertex(478,332);
  curveVertex(400,365);
  endShape();

  beginShape();
  curveVertex(470,300);
  curveVertex(452,353);
  curveVertex(453,337);
  curveVertex(443,337);
  endShape();

  beginShape();
  curveVertex(390,346);
  curveVertex(430,350);
  curveVertex(422,375);
  curveVertex(475,375);
  endShape();

  beginShape();
  curveVertex(474,430);
  curveVertex(464,410);
  curveVertex(460,380);
  curveVertex(550,340);
  endShape();

  //when pressed, the brain lines get bigger
  if (mouseIsPressed){
    brainstrokeW=12;
  }else{
    brainstrokeW=6;
  }

  if (mouseIsPressed){
    brainlines=12;
  }else{
    brainlines=8;
  }

//emoji 2: girl with makeup on half her face

//hair on the back of the head
fill(0);
noStroke();
rect(985,400,432,400);

//head
fill(223,178,139);
noStroke();
ellipse(1200,450,440,490);

  //half ellipse; right side of face
  fill(255,231,210);
  noStroke();
  arc(1200, 450, 440, 490, radians(270), radians(90));

//bangs
fill(0);
noStroke();
arc(1200, 420, 438, 430, radians(180), radians(0));

//left eye
noFill();
strokeWeight(5);
stroke(0);
beginShape();
curveVertex(1135,554);
curveVertex(1135,484);
curveVertex(1049,471);
curveVertex(1049,450);
endShape();

//left pupil
fill(0);
noStroke();
circle(1100,490,25,25);

//right eye
noFill();
strokeWeight(5);
stroke(0);
beginShape();
curveVertex(1257,570);
curveVertex(1257,490);
curveVertex(1343,475);
curveVertex(1343,520);
endShape();

strokeWeight(3);
beginShape();
curveVertex(1283,490);
curveVertex(1283,465);
curveVertex(1325,460);
curveVertex(1325,462);
endShape();

//right pupil
fill(114,193,231);
noStroke();
circle(1293,492,25,25);

//nose left side
noFill();
stroke(207,160,119);
strokeWeight(5);
beginShape();
curveVertex(1160,500);
curveVertex(1170,559);
curveVertex(1198,570);
curveVertex(1200,567);
endShape();

//nose right side
noFill();
stroke(244,217,193);
strokeWeight(5);
beginShape();
curveVertex(1215,570);
curveVertex(1202,570);
curveVertex(1227,555);
curveVertex(1220,460);
endShape();

//blush on cheek; right side
fill(255,200,200,100);
noStroke();
circle(1351,555,55,55);

//mouth left side
noFill();
stroke(0);
strokeWeight(5);
beginShape();
curveVertex(1200,598);
curveVertex(1198,635);
curveVertex(1137,610);
curveVertex(1127,516);
endShape();

//mouth right side
noFill();
stroke(255,184,184);
strokeWeight(5);
beginShape();
curveVertex(1200,598);
curveVertex(1202,635);
curveVertex(1260,610);
curveVertex(1260,520);
endShape();








  console.log(mouseX,mouseY);
}
