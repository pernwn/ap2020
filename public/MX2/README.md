![screenshot](Screenshot 2020-02-15 at 7.45.25 PM.png)

[Link to emojis](https://pernwn.gitlab.io/ap2020/MX2/)

[Link to repository](https://gitlab.com/pernwn/ap2020/-/tree/master/public/MX2)

## **Describe your program and what you have used and learnt.**

My program consists of two emojis in one canvas. It is divided in the middle to distinct between the two, as they are not related to each other.

The first emoji is a smartphone displaying a brain. When the mouse is pressed, the strokeWeight increases in size to indicate that the brain is pulsating.
The background is a dark blue, almost replicating the colour scheme of Facebook and other social media platforms. To make this I used rect and circle for the
phone and a series of vertex and bezierVertex to make the brain. I also used a "let" to create and name a new variable, as I used this for increasing the
strokeWeights on the brain. 

The second emoji is a girl, specifically Asian, with half her face with makeup and the other half without. It is mainly made to represent myself, that is why 
I chose the purple background as it is my favourite colour. I created this with ellipse for the head and rect for the hair on the backside. I also managed to
create a half circle, using arc, to represent the bangs. I had to search the internet for this, and found a discussion forum for programming and got help there. 
For this, I used a series of vertex to create the features on the face and circle for the eyes. 

In making the first emoji I was reluctant in using vertex or bezierVertex, as it seemed overly complicated. But when I figured out how to display the coordinates
on the screen, by using console.log(mouseX,mouseY), I managed to figure out the placement of the lines I wanted to make. I quickly got into a rythm in manipulating the functions by messing around a bit. 
I still haven't figured out which numbers of the functions belongs to a certain point on the stroke.  

## **How would you put your emoji into a wider cultural context that concerns representations, identity, race, social, economics, culture, device politics and beyond?**

When making the phone emoji, my initial plan was to showcase how much of our lives we put on display; on social media and the internet in general. 
The internet and the need for validation controls us to some extent. We feel the need to always put on a show for the world and show the perfect aspects of
our lives. It's also to showcase that the brain in the phone is our own brain, because in this technological world, we gain our knowledge by googling everything. Even in our studies, we are reluctant to 
pick up a physical book, when we can just get a PDF-file. But, when I thought more about it, the brain could also imply that technology, to some extent have become more and more conscious over the years.
We are even creating technology to be conscious and have a mind of their own. 

Initially, with the secon emoji, I wanted to display the Asian makeup culture. As I have seen some videoes, where the girls use a foundation shade much lighter
than their natural skintone. They do this to appear more fair skinned. They also use coloured contactlenses and tape their eyelids to create the illusion of having double eyelids and bigger eyes.
There is much more that I don't really can understand. But I wanted to make this to show how much the western beauty standards affects other cultures, as to what is seen as beautiful. They use a lighter shade to appear "white"
and tape their eyelids to have bigger eyes, as it is typical for Asians to have smaller eyes. Again, when looking at this emoji I saw that it also had another meaning. 
Because I made it to represent myself, I thought of it as showcasing my biracial culture. I am both Thai and Danish, and I am not more one or the other. This deals with the question of diversity in emojis. Thereby, the emoji also represents two different cultures in one person. 





