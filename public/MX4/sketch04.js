let y=320;
let x=950;
let x2=450;
let value=0;
let running;
let gameOver = false;
frames = 0;



function preload(){
  running=createImg("running.gif","stick figure running");
}

function setup() {
  //put setup code here
  print("hello world!");
  createCanvas(950,550);
  frameRate(50);


}
function draw() {
  //put drawing code here
  background(255);
  frames++;
  running.size(60,70);
  running.position(0,y);


  //the ground
  stroke(0);
  strokeWeight(6);
  line(0,400,950,400);

  //the objects the user has to jump over
  strokeWeight(3);
  rectMode(CORNER);
  rect(x,340,70,58,5);
  rect(x2,300,120,98,5);


  //if the x of both rect is less than 0, they return to the width of canvas
  if(x<0){
    x=width;
  }

  if(x2<0){
    x2=width;
  }

  //if x of both rects are less than 60 and the y of stick figure is greater than 270
  //gameOver is true and both frame and value resets to 0
  //and y,x and x2 also resets to their original position
  //otherwise the program runs
  if(x<60 && y>270){
    gameOver = true;
  }

  if(x2<60 && y>270){
    gameOver=true;
  }

  if(gameOver){
    frames = 0;
    value = 0;
    y=320;
    x=950;
    x2=450;
  } else {
    x-=7;
    x2-=7;
  }


textAlign(LEFT);
strokeWeight(0.4);
textSize(15);
text('High score: 17287',800,50);
text('Most jumps: 1294',800,30);

textAlign(CENTER);
textSize(20);
text(frames,500,195);
text(value,500,115);
strokeWeight(1);
text('SCORE',500,170);
text('JUMPS',500,90);

textSize(19);
text('HOW TO PLAY',70,425);
textSize(15);
strokeWeight(0.5);
text('Jump = Spacebar',65,445);
text('Start = Right Arrow',69,465);



if (mouseIsPressed){
  console.log(mouseX,mouseY);
}

}


//when spacebar is pressed the gif moves position on the y axis
function keyPressed(){
  if(keyCode===32){
    y=210;
    value+=1;
  } else {
    if(keyCode===39);
    gameOver = false;
  }
}


//when spacebar is released the gif returns to original position
function keyReleased(){
  if(keyCode===32){
  y=320;
}
}
