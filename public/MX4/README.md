![screenshot](obsession.png)

[Link to Obsession](https://pernwn.gitlab.io/ap2020/MX4/)

[Link to repository](https://gitlab.com/pernwn/ap2020/-/tree/master/public%2FMX4)

## Obsession
We live in a world where numbers are a big part of our lives. Especially in our lives on social media. We constantly strive to be the one with the most like or most followers and we also strive to be better than our peers. I have made this game to reflect how we are obsessed with numbers; having the highest number, the highest value. The objective is to jump over blocks in order for the “jumps” count to increase by one. The longer you can do this, the higher score you’ll get, because when you hit one of the blocks, it’s game over. 

Sometimes we get so obsessed with being the best, that we even always try to beat our own high score. But in doing this, you’ll realise that it serves no purpose in your daily life. The numbers are irrelevant, still we find them important and we strive for higher numbers, as we sometimes associate it with success. The higher, the better or what? 

### Describe your program and what you have used and learnt. 
---
I have made a fairly simple miniex this time, in terms of the style of it. I haven’t used any colours as I wanted to draw more attention the simplicity of a game that might get you obsessed with beating a high score. This being, I wanted to draw more attention to the numbers than the scenery. I have inserted a gif with “createImg” in function preload, and this is the character which the user will be controlling in the game. I used a function keyPressed and within this an if(keycode===32) to make it possible for the user to make the character jump, by pressing the spacebar. I also implemented a function keyReleased so whenever the user lets go of the spacebar, the character will return to its original position.

To make the rectangles loop I used a syntax which I also used in last week’s miniex, which is x-=7 aka x=x-7. This lets the x value of the rect decrease by 7. For making them loop, I used an “if” statement (conditional statement) which said if x is less than 0, x will return to width. 

I had a lot of trouble when I wanted to figure out how to make the game end whenever the character would hit a rectangle. It seems pointless to have the character go straight through the obstacles, because that wouldn’t be a challenge at all. One of the instructors helped me. She started by making a variable; let gameOver = false. Then she made an “if” statement, that if the x value of the rect is less than 60, which is the position of the gif and if the y value of the gif is greater than 270, which is the highest position of the gif above the rects then gameOver will become true. And if gameOver is true, then the frames aka. Score and value aka. Jumps will return to 0 and the y, x and x2 value will also return to their original positions. I would have never thought of that myself and I will definitely keep that in mind for future projects. It was pretty simple once I read the code.
I also wanted to implement a button where the user could choose to who or hide the scores that keeps track of your progress in the game. I wanted the user to have the option to experience the game without any signs of progess and have them feel what it's like. I know from myself, that I would be more aware of the pointlessness of the game if I couldn't keep track of how far I had come. Though, I couldn't quite figure out how to make it do that. So I have to see if I'll get it working another time.


In testing the program before uploading I found a flaw. I want the game to start only when the right arrow key is pressed, keycode 39. When making the changes in Atom it works fine, but in trying the program multiple times, the program will start when pressing any other key. This is something I will have to have a closer look at and figure out where it goes wrong.

### Articulate how your program and thinking address the theme 'capture all'.
---
”If work and leisure became mathematically manageable data units for the assembly line production model of industrial capitalism, today’s workers even offer their free time to the extent that it serves a form of unaware labour facilitated by playful technologies and game-like mechanics.” (https://transmediale.de/content/call-for-works-2015) 

This quote is from the transmediale 2015 call for works. In relation to my program and my thinking I perceive it as we spend our free time building up a kind of façade in trying to fit into a certain standard in society. Look at how much time “influencers” nowadays use their spare time in building “the perfect life”. In a way, their lives become this game where they’re all ranking each other in regard to how many followers and likes they can get. The game I made captures our path on the internet in becoming the best of the best. Every time our number increases, we feel like we have achieved a great deal, even though, in reality this doesn’t really define who we are. But we give it the power to control us and we give it value in which we see importance. 

When a program or a certain media captures our “value”, it makes us behave in certain ways. In relation to my program; you can always choose to only jump when you have to get past the obstacles. But, because of the way we see ranking and high scores, some might try to jump multiple times in between the obstacles to get a higher ranking. 

The “buttons” I have made that the user can interact with offers only one choice of action, but become binary choices (Pold, Buttons in Software Studies, 2008:34), because the user can choose not to interact with any. The right arrow key only allows the user to start the game again, and the spacebar only makes the character jump a certain height. You cannot make the character jump partially or even higher. These are the only two functions that I have implemented, and it forces the user to use exactly these keys in interacting with the program. 


### What are the cultural implications of data capture?
---
There has been a debate about how Facebook stores our data and uses them for advertising purposes and what not. Also, these “cookies” which we see on various sites, is also a way of capturing our behaviour online. People have been very reluctant to agree with these terms as they get paranoid and they feel that their privacy is being violated. Sometimes, when visiting a website, you have to accept the cookies for the pop-up message to disappear. You usually accept them just to have to message go away, as it can take up a lot of space on the screen. This is again an example of how the buttons force you into making binary decisions. You have no other choice but to accept them if you want to continue browsing the webpage. People can become vary and more reluctant in sharing personal stuff. Some might even say that we are always being watches as if we are living in a Big Brother society. 

