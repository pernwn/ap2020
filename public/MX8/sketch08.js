//Defining functions
var data;
var min_stars = 800;


var firstPicRightSide = 100;
var firstPicLeftSide = 240;

var secondPicRightSide = 300;
var secondPicLeftSide = 440;

var thirdPicRightSide = 500;
var thirdPicLeftSide = 640;

var firstRowPicTop = 50;
var firstRowPicBottom = 150;

var sencondRowPicTop = 200;
var secondRowPicBottom = 303;

var thirdRowPicTop = 350;
var thirdRowPicBottom = 450;

var fourthRowPicTop = 500;
var fourthRowPicBottom = 600;

function preload(){
  data = loadJSON("signs.json"); //Collecting the data from our JSON file
  //Star sign pictures
  aries = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/90793311_2739551802939941_2956254023088340992_n.png?_nc_cat=103&_nc_sid=b96e70&_nc_oc=AQk4-sqKkdFFpEy4_90vhEuLHyRkzQYJb4v7PfSeW2B-rdfMAUhzFa6b0dXYBJG1RoI&_nc_ht=scontent.faar2-1.fna&oh=34108370fbd3416816879c33da99eb9a&oe=5EA2B8E6");
  taurus = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/90890299_525477714833857_1459444726292807680_n.png?_nc_cat=110&_nc_sid=b96e70&_nc_oc=AQk3H0R-6Sh4kzzUqdSRIKwMMTR8rx3Z54dDNEaBoNDQTTaW9SzssxMquTHIqeKvLeQ&_nc_ht=scontent.faar2-1.fna&oh=5653aa8ddb3c181da8365772bccc59f0&oe=5EA3FA06");
  gemini = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/91540827_2623660907910622_7985863651024175104_n.png?_nc_cat=105&_nc_sid=b96e70&_nc_oc=AQkTPcbOcwi5qafZIjhtp9ZFZPkqONtnw2uXeltU-0UAOnJibLOBpdE08mC3oCdCTbc&_nc_ht=scontent.faar2-1.fna&oh=e3d3e1f14cbe8ffac3cb2fde8883c89b&oe=5EA4D305");
  cancer = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/90722955_217854889433920_1943647034162544640_n.png?_nc_cat=101&_nc_sid=b96e70&_nc_oc=AQkzI1jMT49bsHjvUPLyr4LdYNVJ3Bn4_Bf-dt_8JeAfyj96uN4dg-TpK7CuV4jFxhU&_nc_ht=scontent.faar2-1.fna&oh=16cde2d20e107e1d526e913a4b616e40&oe=5EA26352");
  leo = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/90705601_919953675101079_5388878011544109056_n.png?_nc_cat=111&_nc_sid=b96e70&_nc_oc=AQnyyxPygs_qvVQT1CHprS1VNbOmGUTbtPiTLP6WSCDWL7jg3usTubpioFpdMS8aj_Q&_nc_ht=scontent.faar2-1.fna&oh=5164f4ef30eb0a989bdafc8929151ed2&oe=5EA57F42");
  virgo = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/90755994_2891870357559485_1301339463934279680_n.png?_nc_cat=103&_nc_sid=b96e70&_nc_oc=AQlLlPBh2ge6o1iybIxpiAO4hRdouRCo89V7WDNQK2oHaZfmko92vywzexQONI3NtmM&_nc_ht=scontent.faar2-1.fna&oh=96e8353d06f5665ef9fc6fbdfff56e43&oe=5EA3E925");
  libra = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/90737397_232296541157678_1592598337490518016_n.png?_nc_cat=107&_nc_sid=b96e70&_nc_oc=AQllA9U89Hl-veTPMqAu_VuMbzhTBJuy4MWk1d6UxyuB18ibSD6Vd5Dudp_TqtFDk1s&_nc_ht=scontent.faar2-1.fna&oh=bdfe9edf554ed3569074e65a6a20598b&oe=5EA2309B");
  scorpio = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/90710109_3214318671921340_1712867347447939072_n.png?_nc_cat=103&_nc_sid=b96e70&_nc_oc=AQmkhEXlTvYVojUno9iiAK7iH5TihTTs8689xHz7EewJaHPQ87_o7v91vxYIIxn0ncc&_nc_ht=scontent.faar2-1.fna&oh=bc2e676d41ff789f174cfe8d27a3f714&oe=5EA2D551");
  sagittarius = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/90710107_1571284163029814_3776307547846213632_n.png?_nc_cat=103&_nc_sid=b96e70&_nc_oc=AQlYcrHs6U8MitWmh2edBA8PxQHJEzBn2yxPe0mEq6KWTVrJ0IPDkwBNS7zY9D8YlzA&_nc_ht=scontent.faar2-1.fna&oh=66f3a3bf95bb34a74523e168f02ac21f&oe=5EA38C92");
  capricorn = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/90963077_148706579832470_2685280052437843968_n.png?_nc_cat=102&_nc_sid=b96e70&_nc_oc=AQlVnz68dFs0zDkEnEJNg6Cp27JMDEAlPVclonBwMiKe4yBXWcFgg2nEuIyOmBhT-F0&_nc_ht=scontent.faar2-1.fna&oh=24d146373818370903a92bc4477d22d6&oe=5EA24F5D");
  aquarius = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/90903785_1426982727482975_1275825253662064640_n.png?_nc_cat=101&_nc_sid=b96e70&_nc_oc=AQkjTXNggs9yyitKpLL8XCznqQVRqGAN31XLd9fB14XPghCWyHuPrr54WovWyFcpzTc&_nc_ht=scontent.faar2-1.fna&oh=1adf541985a8729e20ccc18ba92d5fc7&oe=5EA48C16");
  pisces = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/90711149_214177506486744_4801604889526403072_n.png?_nc_cat=108&_nc_sid=b96e70&_nc_oc=AQmivBNy1iT-uuzpC0hQ3m3ijgjaYNwobbO2-pPbowPBvEPydcqay4qHXRDk-GCV1-I&_nc_ht=scontent.faar2-1.fna&oh=7165e5d0b3954932c88f445aca219646&oe=5EA44090");
}

function setup() {
  //put setup code here
  createCanvas(1400,700);
  background(10,4,89);

//creating the stars for the background
  for (var star = 0; star < min_stars; star++){
    noStroke();
    fill(255);
    circle(random(10,850), random(10,1000), random(1,3));
  }

}
function draw() {
  //put drawing code here


image(aries,100,50);
image(taurus,300,50);
image(gemini,500,50);
image(cancer,100,200);
image(leo,300,200);
image(virgo,500,200);
image(libra,100,350);
image(scorpio,300,350);
image(sagittarius,500,350);
image(capricorn,100,500);
image(aquarius,300,500);
image(pisces,500,500);


//draws a rectangle on the right side
fill(10,4,89);
noStroke();
rect(750,0,1000,700);

fill(255);
textSize(30);
text("Show me a star sign!", 940,50);


//show the aries text
if(mouseX > firstPicRightSide && mouseX < firstPicLeftSide && mouseY > firstRowPicTop && mouseY < firstRowPicBottom && mouseIsPressed){
textSize(15);
noStroke();
fill(255);
  text(data.showMeA[0].starSign,1055,100);
  text(data.showMeA[0].date,1035,120);
  text(data.showMeA[0].horoscope[0],850,170);
  text(data.showMeA[0].horoscope[1],850,190);
  text(data.showMeA[0].horoscope[2],850,210);
  text(data.showMeA[0].horoscope[3],850,230);
  text(data.showMeA[0].horoscope[4],850,250);
  text(data.showMeA[0].horoscope[5],850,270);
}

//show taurus text
if(mouseX > secondPicRightSide && mouseX < secondPicLeftSide && mouseY > firstRowPicTop && mouseY < firstRowPicBottom && mouseIsPressed){
textSize(15);
noStroke();
fill(255);
  text(data.showMeA[1].starSign,1055,100);
  text(data.showMeA[1].date,1035,120);
  text(data.showMeA[1].horoscope[0],820,170);
  text(data.showMeA[1].horoscope[1],820,190);
  text(data.showMeA[1].horoscope[2],820,210);
  text(data.showMeA[1].horoscope[3],820,230);
  text(data.showMeA[1].horoscope[4],820,250);
  text(data.showMeA[1].horoscope[5],820,270);
}

//show gemini text
if(mouseX > thirdPicRightSide && mouseX < thirdPicLeftSide && mouseY > firstRowPicTop && mouseY < firstRowPicBottom && mouseIsPressed){
textSize(15);
noStroke();
fill(255);
  text(data.showMeA[2].starSign,1055,100);
  text(data.showMeA[2].date,1035,120);
  text(data.showMeA[2].horoscope[0],800,170);
  text(data.showMeA[2].horoscope[1],800,190);
  text(data.showMeA[2].horoscope[2],800,210);
  text(data.showMeA[2].horoscope[3],800,230);
  text(data.showMeA[2].horoscope[4],800,250);
}

//show cancer text
if(mouseX > firstPicRightSide && mouseX < firstPicLeftSide && mouseY > sencondRowPicTop && mouseY < secondRowPicBottom && mouseIsPressed){
textSize(15);
noStroke();
fill(255);
  text(data.showMeA[3].starSign,1055,100);
  text(data.showMeA[3].date,1035,120);
  text(data.showMeA[3].horoscope[0],850,170);
  text(data.showMeA[3].horoscope[1],850,190);
  text(data.showMeA[3].horoscope[2],850,210);
  text(data.showMeA[3].horoscope[3],850,230);
  text(data.showMeA[3].horoscope[4],850,250);
  text(data.showMeA[3].horoscope[5],850,270);
  text(data.showMeA[3].horoscope[6],850,290);
  text(data.showMeA[3].horoscope[7],850,310);
}

//show leo text
if(mouseX > secondPicRightSide && mouseX < secondPicLeftSide && mouseY > sencondRowPicTop && mouseY < secondRowPicBottom && mouseIsPressed){
textSize(15);
noStroke();
fill(255);
  text(data.showMeA[4].starSign,1055,100);
  text(data.showMeA[4].dates,1035,120);
  text(data.showMeA[4].horoscope[0],800,170);
  text(data.showMeA[4].horoscope[1],800,190);
  text(data.showMeA[4].horoscope[2],800,210);
  text(data.showMeA[4].horoscope[3],800,230);
  text(data.showMeA[4].horoscope[4],800,250);
}

//show virgo text
if(mouseX > thirdPicRightSide && mouseX < thirdPicLeftSide && mouseY > sencondRowPicTop && mouseY < secondRowPicBottom && mouseIsPressed){
textSize(15);
noStroke();
fill(255);
  text(data.showMeA[5].starSign,1055,100);
  text(data.showMeA[5].date,1035,120);
  text(data.showMeA[5].horoscope[0],770,170);
  text(data.showMeA[5].horoscope[1],770,190);
  text(data.showMeA[5].horoscope[2],770,210);
  text(data.showMeA[5].horoscope[3],770,230);
  text(data.showMeA[5].horoscope[4],770,250);
  text(data.showMeA[5].horoscope[5],770,270);
}

//show libra text
if(mouseX > firstPicRightSide && mouseX < firstPicLeftSide && mouseY > thirdRowPicTop && mouseY < thirdRowPicBottom && mouseIsPressed){
textSize(15);
noStroke();
fill(255);
  text(data.showMeA[6].starSign,1055,100);
  text(data.showMeA[6].date,1035,120);
  text(data.showMeA[6].horoscope[0],790,170);
  text(data.showMeA[6].horoscope[1],790,190);
  text(data.showMeA[6].horoscope[2],790,210);
  text(data.showMeA[6].horoscope[3],790,230);
  text(data.showMeA[6].horoscope[4],790,250);
}

//show scorpio text
if(mouseX > secondPicRightSide && mouseX < secondPicLeftSide && mouseY > thirdRowPicTop && mouseY < thirdRowPicBottom && mouseIsPressed){
textSize(15);
noStroke();
fill(255);
  text(data.showMeA[7].starSign,1055,100);
  text(data.showMeA[7].date,1035,120);
  text(data.showMeA[7].horoscope[0],770,170);
  text(data.showMeA[7].horoscope[1],770,190);
  text(data.showMeA[7].horoscope[2],770,210);
  text(data.showMeA[7].horoscope[3],770,230);
  text(data.showMeA[7].horoscope[4],770,250);
  text(data.showMeA[7].horoscope[5],770,270);
  text(data.showMeA[7].horoscope[6],770,290);
  text(data.showMeA[7].horoscope[7],770,310);
}

//show sagittarius text
if(mouseX > thirdPicRightSide && mouseX < thirdPicLeftSide && mouseY > thirdRowPicTop && mouseY < thirdRowPicBottom && mouseIsPressed){
textSize(15);
noStroke();
fill(255);
  text(data.showMeA[8].starSign,1055,100);
  text(data.showMeA[8].date,1050,120);
  text(data.showMeA[8].horoscope[0],790,170);
  text(data.showMeA[8].horoscope[1],790,190);
  text(data.showMeA[8].horoscope[2],790,210);
  text(data.showMeA[8].horoscope[3],790,230);
  text(data.showMeA[8].horoscope[4],790,250);
}

//show capricorn text
if(mouseX > firstPicRightSide && mouseX < firstPicLeftSide && mouseY > fourthRowPicTop && mouseY < fourthRowPicBottom && mouseIsPressed){
textSize(15);
noStroke();
fill(255);
  text(data.showMeA[9].starSign,1055,100);
  text(data.showMeA[9].date,1050,120);
  text(data.showMeA[9].horoscope[0],800,170);
  text(data.showMeA[9].horoscope[1],800,190);
  text(data.showMeA[9].horoscope[2],800,210);
  text(data.showMeA[9].horoscope[3],800,230);
  text(data.showMeA[9].horoscope[4],800,250);
  text(data.showMeA[9].horoscope[5],800,270);
  text(data.showMeA[9].horoscope[6],800,290);
  text(data.showMeA[9].horoscope[7],800,310);
}

//show aquarius text
if(mouseX > secondPicRightSide && mouseX < secondPicLeftSide && mouseY > fourthRowPicTop && mouseY < fourthRowPicBottom && mouseIsPressed){
textSize(15);
noStroke();
fill(255);
  text(data.showMeA[10].starSign,1055,100);
  text(data.showMeA[10].date,1040,120);
  text(data.showMeA[10].horoscope[0],780,170);
  text(data.showMeA[10].horoscope[1],780,190);
  text(data.showMeA[10].horoscope[2],780,210);
  text(data.showMeA[10].horoscope[3],780,230);
  text(data.showMeA[10].horoscope[4],780,250);
}

//show pisces text
if(mouseX > thirdPicRightSide && mouseX < thirdPicLeftSide && mouseY > fourthRowPicTop && mouseY < fourthRowPicBottom && mouseIsPressed){
textSize(15);
noStroke();
fill(255);
  text(data.showMeA[11].starSign,1055,100);
  text(data.showMeA[11].date,1030,120);
  text(data.showMeA[11].horoscope[0],760,170);
  text(data.showMeA[11].horoscope[1],760,190);
  text(data.showMeA[11].horoscope[2],760,210);
  text(data.showMeA[11].horoscope[3],760,230);
  text(data.showMeA[11].horoscope[4],760,250);
  text(data.showMeA[11].horoscope[5],760,270);
  text(data.showMeA[11].horoscope[6],760,290);
}




if (mouseIsPressed){
  console.log(mouseX,mouseY);
}
}
