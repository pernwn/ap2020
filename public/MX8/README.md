![screenshot](ShowMeAStarSign.png)

[Link to program](https://pernwn.gitlab.io/ap2020/MX8/)

[Link to repo](https://gitlab.com/pernwn/ap2020/-/tree/master/public/MX8)

## Made in collaboration with Anne Nielsen

[Link to Anne's repo](https://gitlab.com/AnneNielsen/ap-2020-projects/-/tree/master/public/MiniEx8)

## For this MiniEx the objectives was::

*  To understand how JSON works technically, in terms of storing data and how data can be retrieved via code.

*  To reflect upon the aesthetics of code and language, as well as the multiple aural dimensions of an e-lit.

## Show me a star sign!
The program we have created for this week's MiniEx is called "Show me a star sign!". It displays the horoscopes for the year 2020. 
When the mouse is being clicked on a horoscope sign on the left side of the screen, a description for the chosen star sign will appear on the right side of the screen. The description contains the name of the star sign, the dates the star sign covers and then the description. 
The dark blue background symbolizes the night sky. The white circles’ size is randomly chosen between numbers 1 and 3, and they are also randomly positioned on the x- and y-axis, as to symbolize bright shining stars. The stars are only positioned on the left side of the canvas, so it’s easier to read the horoscopes on the right side. 

## Our inspiration sources for this program
To create this program we gained inspiration from different sources.  
In terms of creating our overall structure and setup for our sketch file, we gained inspiration from another through a programmer on p5.js Web Editor called kimagosto (https://editor.p5js.org/kimagosto/sketches/ryr40qHJ4). To figure out how to set up and create our own JSON file, we gained inspiration through another user's JSON file on GitHub called isaichenko (https://github.com/isaichenko/horoscopeJSON/blob/master/signs.json). This JSON file contained most of our overall structure, since it is made on star signs- so we adjusted it to our program's style and language. To make sure our connection between the JavaScript sketch and the JSON file was correct, we used Daniel Shiffman's online p5.js Tutorial´s nr. 10.2 & 10.3, The Coding Train. 
Neither one of us are fortune tellers, so we collected the horoscopes online as well and adjusted them to our program (https://www.horoscope.com/us/horoscopes/yearly/2020-horoscope-overview.aspx). This page was also where we got the star sign pictures from. 

When programming we learned how to create, edit (the overall structure) and make an array for our longer texts in our array for all the star signs in the JSON file. 
This also reflects that this week’s MiniEx main focus was to work with a JSON file and text as the main medium. 
In our program text takes various forms: the written code languages (JavaScript and JSON) and text through images (text printed on pictures). The texts are written in different languages, all expressing something different. You can maybe even argue that the star-horoscopes is a language in itself, maybe as a category under the human language? 

One thing that is important to remember, is that the code and the written languages is not the whole experience of the program,- like they say in “The Aesthetics of Generative Code”:
”… the aesthetic value of code lies in its execution, not simply its written form” (Geoff Cox, Alex McLean, and Adrian Ward. "The Aesthetics of Generative Code.", 2001, pp. 1). 

In “Vocable Code” they agree upon this:  ”Indeed, saying words or running code or simply understanding how they work is not enough in itself. What is important is the relation to the consequences of that action, and that is why the analogy of speech works especially well…” (Geoff Cox, and Alex McLean. Speaking Code, 2013. pp. 38).  

This explains why a program shouldn't just be written, it should be read, interpreted by the user or reader and executed to express its full value, performativity, expressivity, embodiment and its aesthetic value. You have to put programming in a bigger cultural context to see its full potential and value. Interpret, twist and rethink the written code, - that adds a whole dimension to the program. 

“…stress the programming procedures that lie behind the raw code that in themselves can sense or think nothing” (Geoff Cox, Alex McLean, and Adrian Ward. "The Aesthetics of Generative Code.", 2001, pp. 3). 

In the two texts mentioned above, a big focus in both of them is coding relation to poetics and poetry. Our written code this week was very functional at first,- for us to be aware of what the different elements do and to make our program work. When we finished the code and had incorporated all the elements we wanted, we edited in the code to make it more poetic. We changed many of the names for our defined functions and added some defined variables for the positions of our pictures. An example of this is the name for our array for all of our star signs,- we called this “showMeA”, like the title of our work. This also emphasizes that code shouldn't just be read but interpreted, executed and shown like mentioned earlier. 
Would you interpret and evaluate our code as poetry or poetic? 


## Sources
* Inspiration to setup - code structure: https://editor.p5js.org/kimagosto/sketches/ryr40qHJ4
* Where we collected the horoscopes from and the pictures (screenshot size 2,4 cm x 1,8 cm): https://www.horoscope.com/us/horoscopes/yearly/2020-horoscope-overview.aspx 
* Where we got the structure for our code: https://github.com/isaichenko/horoscopeJSON/blob/master/signs.json 
* Daniel Shiffman, 10.2: What is JSON? Part I - p5.js Tutorial [online], The Coding Train: https://www.youtube.com/watch?v=_NFkzw6oFtQ&list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r&index=3&t=0s
* Daniel Shiffman, 10.3: What is JSON? Part II - p5.js Tutorial, The Coding Train, [online]: https://www.youtube.com/watch?v=118sDpLOClw&list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r&index=3 
* Geoff Cox, and Alex McLean. Speaking Code. Cambridge, Mass.: MIT Press, 2013. 17-38. (The chapter Vocable Code)
* Geoff Cox, Alex McLean, and Adrian Ward. "The Aesthetics of Generative Code." Proc. of Generative Art. 2001.

