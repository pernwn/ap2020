![Screenshot](watching.png)

[Throbber](https://pernwn.gitlab.io/ap2020/MX5/)

[Repository](https://gitlab.com/pernwn/ap2020/-/tree/master/public%2FMX5)

I wasn’t fully satisfied with my throbber, so I took this opportunity to create an entirely new one. With this throbber I tried to implement some ideas from Data Capture. The throbber has two parts. There’s an ellipse in the middle rotating and it looks like there are multiple ellipses following each other. Then there’s a single ellipse, going around the middle, creating a “path”. Thus, creating a shape of an eye. My idea is that, when waiting for a webpage to load, the site is kind of gathering data and the internet etc is watching you. 

I wanted it to be mesmerizing so it’s pleasing to watch, but the colours convey a different meaning. The red throbber on a dark background, makes it seem more ominous and a little intimidating. I also wanted the red to symbolise danger or error. It’s kind of an eye that hypnotises you into staying or to continue your mindless internet surfing. The thing is though, that is keeps on watching you, even when you can’t see it, it’s always lurking in the background. 
