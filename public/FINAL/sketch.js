
let button;
let buttonYes;
let buttonNo;
let inp;

var div1;
var div2;

var meal = false;

var meal2 = false;

var meals;

let waiter;
let robotwaiter;

var api = 'https://www.themealdb.com/api/json/v1/1/random.php?s=';
var apiKey = "&APPID=1";

var dish = [];

let sound;
let backgroundN;

function preload(){
//Loading the images which are to be displayed also loading the sound files

  res = loadImage('restaurantsetting.jpeg');

  waiter = loadImage("waiter.png");
  robotwaiter = loadImage("robotwaiter.png"); 

  soundFormats('mp3', 'ogg');
  sound = loadSound('glitchsound.mp3');
  backgroundN = loadSound('BGnoise.mp3');

}

function setup() {
/* The main screen of the program.

Creates a canvas

The input is created – position and size is determined
 – costumization of the font size when the user is writing
 – costumization of the font family

The button is created – postition and size is determined
 – added mousePressed to create the function showMeal
 – costumization of the button - styling

The buttons "yes" and "no" are also created in setup
 – These will be hidden here and shown for later use

Two div is created here which will be changed in the other functions with .html
  div1 and div2

*/
    createCanvas(1450,730);
    frameRate(0.9)

    /*this refers to the function createBackground(); whereas the creation of the
      the speechbubble and the position of the gif is placed. The image of the restaurantsetting
      is also called in this function.*/
    createBackground();
    image(waiter, -90,50,1350,750);


    //text in speech bubble
    div1 = createDiv('Hello and welcome');
    div1.position(600,120);
    div1.size(500);
    div1.style("font-size", "25px");

    div2 = createDiv('');
    div2.position(570,145);
    div2.size(600);
    div2.style("font-size", "25px");


    //letting the user make a decision
    divOrder = createP("Would you like this dish?");
    divOrder.position(500,600);
    divOrder.style("font-size","40px");

    divOrder.hide();

  //creating input bar
    inp = createInput('');
    inp.changed(askingForMeal);
    inp.position(700,357);
    inp.size(300,40);

  //styling input
    inp.style("font-size", "20px");
    inp.style("font-family", "Miller", "serif");

  //creating button
    button = createButton("Ask for meal");
    button.position(750,410);
    button.size(200,30);
    button.mousePressed(showMeal);

  //styling button
    button.style("background");
    button.style("border", "2");
    button.style("font-family", "Miller", "serif");
    button.style("font-size", "20px");
    button.style("font-style","italic");
    button.style("cursor", "pointer");


  //creating more buttons – the "yes" and "no" buttons
    buttonYes = createButton("Yes");
    buttonYes.position(1000,640);
    buttonYes.size(100,40);
    buttonYes.style("font-size", "20px");
    buttonYes.style("cursor", "pointer");
    buttonYes.mousePressed(yes);

    buttonNo = createButton("No");
    buttonNo.position(1150,640);
    buttonNo.size(100,40);
    buttonNo.style("font-size", "20px");
    buttonNo.style("cursor", "pointer");
    buttonNo.mousePressed(no);


  //hiding the yes and no buttons in setup – will be shown in a later function
    buttonNo.hide();
    buttonYes.hide();

  //the background music is played
    backgroundN.play();
  //setting the volume of the music
    backgroundN.setVolume(0.05);
  //making the music loop when it finishes
    backgroundN.loop();


}

function draw() {
/*This is created for the glitch effect.

A boolean expression is made.
meal is set to false (calling function before setup and preload)

-- if meal is false the glitch will show with the odds of 20 to 100
    it will pick a random number between 0 and 100 – if the new number which is drawn is bigger than 20
    the normal waiter will show, if it's less than 20 the glitch will show

*/
  if(!meal){
    var show = random(100) > 20;
    meal2 = true;

      if(show){
        createBackground();

        image(waiter, -90,50,1350,750);
        div2.html('What meal would you like?');

        sound.stop();

      }else{
        createBackground();

        image(robotwaiter, -90,50,1350,750);
        div2.html('What data would you like?');

        sound.play();

      }
    }

    if(!meal2){
      var show = random(100) > 20;
      meal = true;

        if(show){
          createBackground();

          image(waiter, -90,50,1350,750);
          div2.html('Enjoy your meal!');

          sound.stop();

        }else{
          createBackground();

          image(robotwaiter, -90,50,1350,750);
          div2.html('Enjoy your data!');

          sound.play();

        }
      }

  }

function askingForMeal(){
/* This happens when the user types anything in the input.

The program will write in the console that the user's input doesn't matter,
as the "waiter"/API will give the user a random dish.
   – this is because of the API – it generates a random dish by itself

 */

  console.log("Your input doesn't matter –", this.value());


}

function showMeal(){

  /* This is the outcome of pressing the button "Ask for meal".

  The buttons "yes" and "no" are shown in this function

  A white rectangle will appear on the right side; this is the "menu" on which the dishes
  will be displayed.

  Button, input and the previous texts (div1 and div2) have been hidden.

  */

  meal = true;
  meal2 = true;

  var url = api + inp.value(), apiKey;
  loadJSON(url, gotData);

  //the "menu"
  fill(255);
  noStroke();
  rect(470,40,850,670);


  buttonNo.show();
  buttonYes.show();

  divOrder.show();

//hiding elements
  button.hide();
  inp.hide();
  div1.hide();
  div2.hide();


}

function gotData(data){
/* This is for getting the data from the JSON file

The data we have chosen:
 – strMeal = Shows the name of the dish
 – strMealThumb = Show the picture of the dish

We have then costumized the text and picture size to fit onto the canvas

*/

  var food = data.meals;

//To clear the previous search and show the new shearch:
  for (var j = 0; j < dish.length; j++) {
    dish[j].hide();
  }

  dish = [];

  for(var i = 0; i < food.length; i++){
    //showing the name of the dish

      //append is like "push" – it pushes the meal into the array
      /*Description from p5.js reference:
          – Adds a value to the end of an array. Extends the length of the array by one.*/
    append(dish,createP(food[i].strMeal));
    dish[dish.length-1].position(500,20);
    dish[dish.length-1].style("font-size","40px");

    //showing picture
    append(dish,createImg(food[i].strMealThumb, food[i].strMeal));
    dish[dish.length-1].position(680,140);
    dish[dish.length-1].size(450,450);


}

}

function no() {
/*
  – div2 is now "What would you like instead"
        – Changed with the .html
  – buttonNo and buttonYes are hidden to display the input and button(ask for meal) again
  – div1 and divOrder is also hidden
  – a for-loop has been added to go through the JSON file and hide the elements
    which were shown (the picture and name of the dish)
*/

  createBackground();
  image(waiter, -90,50,1350,750);

  //new text in speech bubble
  div2.html('What would you like instead?');
  div2.position(550,145)

  //meal is set to true — it will not execute the conditional statement
  meal2 = true;
  meal = true;

  button.show();
  inp.show();
  div2.show();

  buttonNo.hide();
  buttonYes.hide();

  divOrder.hide();
  div1.hide();

  //To clear the previous search and show the new shearch:
  for (var j = 0; j < dish.length; j++) {
    dish[j].hide();
  }


}

function createBackground() {
  /*The speechbubble is created with an ellipse and a triangle.*/

    createCanvas(1450,730);
    image(res, 0, 0,1450,750);

  //speechbubble
    noStroke();
    fill(255);
    ellipse(700,150,330,150)
    triangle(552, 153, 574, 184, 316, 250);

}

function yes(){
  /*

    – The waiter is now saying "enjoy your meal" or "enjoy your meal" as an effect of pressing 'yes'
      div2 = Enjoy your data! / Enjoy your meal!

    – buttonNo and buttonYes are hidden
    – divOrder is also hidden
    – a for-loop has been added to go through the JSON file and hide the elements
      which were shown (the picture and name of the dish)
    – input and button have been removed
  */

    createBackground();
    image(waiter, -90,50,1350,750);

  //meal is set to true — it will not execute the conditional statement
    meal = true;

  //meal2 is set to false – it will then execute this conditional statement
    meal2 = false;


    div2.position(610,140)
    div2.show();

    buttonNo.hide();
    buttonYes.hide();
    inp.hide();
    divOrder.hide();

    //To clear the previous search and show the new shearch:
    for (var j = 0; j < dish.length; j++) {
      dish[j].hide();
    }

}
